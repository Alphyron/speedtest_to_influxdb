# Measure your ISPs speed and visualize it with Grafana

This tool helps you to gather information about the stability and speed of your Internet connection by using the Ooklas new CLI Speedtestclient.

This tool does NOT use the `speedtest-cli` package, use the [offical Ookal Speedtest CLi](https://www.speedtest.net/de/apps/cli).

---

There are two ways to use this tool:

  - [1. Run the script directly](#1-run-the-script-directly)
    - [Dependencies](#dependencies)
    - [Configuartion](#configuartion)
      - [Config with arguments](#config-with-arguments)
      - [Config with environment variables](#config-with-environment-variables)
      - [Config in script](#config-in-script)
    - [Crontab](#crontab)
  - [2. Use docker](#2-use-docker)
    - [config](#config)
    - [Crontab](#crontab-1)
  - [EXTRA: Grafana](#extra-grafana)

## 1. Run the script directly

### Dependencies

You need to install InfluxDBClient via pip

```sh
pip install influxdb
```

ore

```sh
pip install -r requirements.txt
```

You also need Ookla Speedtest CLI, you can download it directly from their [Website](https://www.speedtest.net/de/apps/cli) or use your package manager. Again this is not speedtest-cli!

AUR: `ookla-speedtest-bin`

### Configuartion

Currently, you can configure this script in three different ways:

1. command-line arguments
2. environment variables
3. directly in the script

IMPORTANT: The scripts reads it exactly this way.

#### Config with arguments

You can add these arguments and flags:

Argument:

```shell
# Database config
--influx-host [hostname/ip]
--influx-port [port, uses default port 8086 if not specified ]
--influx-username [username]
--influx-password [password]
--influx-dbname [database name]
```

```shell
# Tagging
--tag-host [YOUR HOST]
--tag-region [YOUR REGION]
--tag-location [YOUR REGION]
```

Flags:

```shell
--save-json
--internal-cron #activates the internal cron function (every hour). 
```

#### Config with environment variables

```shell
# dont export these if not necessary
export SAVE_JSON=[TRUE/FALSE]
export INTERNAL_CRON=[TRUE/FALSE]
```

```shell
export INFLUX_HOST=[hostname/ip]
export INFLUX_PORT=[port, uses default port 8086 if not specified ]
export INFLUX_USERNAME=[username]
export INFLUX_PASSWORD=[password]
export INFLUX_DBNAME=[database name]

export TAG_HOST=[host]
export TAG_REGION=[region]
export TAG_LOCATION=[region]
```

#### Config in script

Open the Python file and uncomment and change following settings

```python
#### config ####
config = {
    #	"save_json": False,
    #   "internal_cron" False:

    #	"influx_host": None,
    #	"influx_port": 8086,
    #	"influx_username": None,
    #	"influx_password": None,
    #	"influx_dbname": None,

    # "tag_host": None,
    # "tag_region": None,
    # "tag_location": None,
}
###############
```

Set the `save_json` variable to `True` to export the generated JSON file, the rest is self-explanatory.

### Crontab

Create a crontab with `crontab -e` and paste in the following line:
IMPORTANT: Change the path of the `speedtest.py` accordingly to your location!

```shell
0 * * * * python3 ~/speedtest_to_DB/speedtest.py >/dev/null 2>&1
```

This Cron will execute the script every hour.

## 2. Use docker

CLone this repository and change the following entries in the `docker-compose.yml`

### config

```yml
 environment:
      - INFLUX_HOST=
      - INFLUX_PORT=8086
      - INFLUX_USERNAME=
      - INFLUX_PASSWORD=
      - INFLUX_DBNAME=
      - TAG_HOST=
      - TAG_REGION=
      - TAG_LOCATION=
      # Remove if not necessary
      - INTERNAL_CRON=True
      - SAVE_JSON=False
      # TImeZone
      - TZ=Europe/Berlin
```

after that run `docker-compose up -d` to start the container.

### Crontab

Create a crontab with `sudo crontab -e` and paste in the following line:

IMPORTANT: Change the path of the `docker-compose.yml` accordingly to your location!

```shell
0 * * * * docker-compose -f docker-compose.yml -d up >/dev/null 2>&1
```

This Cron will execute the script every hour.

## EXTRA: Grafana

In this repo, you can find a premade dashboard for Grafana that displays relevant information. Import the `dashboard.json` file you find in here.

![Grafana dashboard](dashboard.png "Grafana dashboard")
