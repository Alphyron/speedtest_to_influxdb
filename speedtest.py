from subprocess import Popen, PIPE
import json
import influxdb
import os
import argparse
import logging
import requests
from time import sleep
from influxdb import InfluxDBClient
from apscheduler.schedulers.blocking import BlockingScheduler

#### config ####
config = {
    #	"save_json": False,
    #   "internal_cron" False:

    #	"influx_host": None,
    #	"influx_port": 8086,
    #	"influx_username": None,
    #	"influx_password": None,
    #	"influx_dbname": None,

    # "tag_host": None,
    # "tag_region": None,
    # "tag_location": None,

}
###############
CONFIG_META = {
    "save_json": {"type": bool, "default": False},
    "internal_cron": {"type": bool, "default": False},
    "influx_host": {"type": str, "default": ""},
    "influx_port": {"type": int, "default": 8086},
    "influx_username": {"type": str, "default": ""},
    "influx_password": {"type": str, "default": ""},
    "influx_dbname": {"type": str, "default": ""},

    "tag_host": {"type": str, "default": ""},
    "tag_region": {"type": str, "default": ""},
    "tag_location": {"type": str, "default": ""},

}

parser = argparse.ArgumentParser(
    description="Runs a speedtest using Ookla's speedtest-cli and reports the results to an influxdb server.")
parser.add_argument(
    "--save-json",
    action="store_const",
    const=True,
    dest="save_json",
    help="if present dumps the JSON output of speedtest-cli to speedtest.json"
)
parser.add_argument(
    "--internal-cron",
    action="store_const",
    const=True,
    dest="internal_cron",
    help="if present enables the internal cron (set to every hour)"
)
parser.add_argument(
    "--influx-host",
    action="store",
    dest="influx_host",
    metavar="HOSTNAME"
)
parser.add_argument(
    "--influx-port",
    action="store",
    dest="influx_port",
    metavar="PORT",
    type=int,
)
parser.add_argument(
    "--influx-username",
    action="store",
    dest="influx_username",
    metavar="USERNAME"
)
parser.add_argument(
    "--influx-password",
    action="store",
    dest="influx_password",
    metavar="PASSWORD"
)
parser.add_argument(
    "--influx-dbname",
    action="store",
    dest="influx_dbname",
    metavar="DBNAME"
)
parser.add_argument(
    "--tag-host",
    action="store",
    dest="tag_host",
    metavar="HOST"
)
parser.add_argument(
    "--tag-region",
    action="store",
    dest="tag_region",
    metavar="REGION"
)
parser.add_argument(
    "--tag-location",
    action="store",
    dest="tag_location",
    metavar="LOCATION"
)
cmd_args = vars(parser.parse_args())
env = os.environ


def coalesce_arg(key):
    if cmd_args.get(key) is not None:
        return cmd_args[key]
    elif config.get(key) is not None:
        return config[key]
    elif env.get(key.upper()) is not None:
        return env[key.upper()]
    else:
        return CONFIG_META[key]["default"]


CONFIG = {k: v["type"](coalesce_arg(k)) for k, v in CONFIG_META.items()}

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s',)


def check_connection():
    url = "http://google.com"
    timeout = 5
    try:
        _ = requests.get(url, timeout=timeout)
        logging.info(msg="Internet connection established!")
        return True
    except requests.ConnectionError:
        logging.error(msg="Internet connection can not be established!")
        return False


def get_json_from_speedtest():
    logging.info(msg="Starting speedtest...")
    # Runs Speedtest CLI, outputs a json and uses a specific server
    process = Popen(["speedtest", "-f", "json", "--accept-license",
                     "--accept-gdpr"], stdout=PIPE)
    (output, err) = process.communicate()
    exit_code = process.wait()

    # converts the output of the Speedtest CLI in a usable json format
    result_json = json.loads(output.decode("utf-8").strip())
    logging.info(msg="Speedtest complete")
    return result_json


def save_json_file(speedtest_result_json):
    logging.info(msg="Writing JSON output")
    with open("speedtest.json", "w") as json_file:
        json.dump(speedtest_result_json, json_file, indent=4)


def send_json_to_db(speedtest_result_json):
    sleep(5)
    # expactat json format for influxDB
    influxdb_json_body = [
        {

            "measurement": "Speedtest",
            "time": speedtest_result_json["timestamp"],
            "tags": {
                "host": CONFIG["tag_host"],
                "region": CONFIG["tag_region"],
                "location": CONFIG["tag_location"],
                "isp": speedtest_result_json["isp"]

            },
            "fields": {
                # *8 for bits converstion
                "download": speedtest_result_json["download"]["bandwidth"]*8,
                # *8 for bits converstion
                "upload": speedtest_result_json["upload"]["bandwidth"]*8,
                "ping": speedtest_result_json["ping"]["latency"],
                "packetLoss": float(speedtest_result_json["packetLoss"]),
                "result_id": speedtest_result_json["result"]["id"]
            }
        }
    ]

    # creates connection to influx and uploads the json data
    client = InfluxDBClient(CONFIG["influx_host"], CONFIG["influx_port"],
                            CONFIG["influx_username"], CONFIG["influx_password"], CONFIG["influx_dbname"])
    logging.debug(msg="Database connection successful")
    client.create_database(CONFIG["influx_dbname"])
    client.write_points(influxdb_json_body)
    logging.info(msg="Data upload successful")


def speedtest():
    if check_connection():
        speedtest_result_json = get_json_from_speedtest()
        send_json_to_db(speedtest_result_json)
        if CONFIG["save_json"]:
            save_json_file(speedtest_result_json)
    else:
        logging.info(msg="Waiting for the next cron to fire!")


if CONFIG["internal_cron"]:
    speedtest()
    scheduler = BlockingScheduler()
    scheduler.add_job(speedtest, 'cron', minute=0, second=0)
    scheduler.start()
else:
    speedtest()
